
/*
 * This program demonstrates work with files and binary trees.
 *
 * Implemented in scope of course
 *     "C for Everyone: Structured Programming", Week 4 Honors
 *     https://www.coursera.org/learn/c-structured-programming
 *
 * Usage:
 *     The program will generate file DATA_FILE with the data
 *     in the current directory, read its content to array
 *     and use this array to build a binary tree.
 *
 * Note:
 *     This version uses recursion, which is unsafe and inefficient
 *     (high memory footprint).
 *
 * Author: Yurii Spichak (Lviv, Ukraine)
 * Date: Jul-26-2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

/********************************** Defines **********************************/
// File with data
#define DATA_FILE      "w4honors.dat"

// Number of data elements in a file
#define DATA_COUNT     6

// Data range
#define MIN_VAL        0   // Including
#define MAX_VAL        100 // Excluding

// Debug macros
#define DBG
//#define DBG(s)         printf("%4d: ", __LINE__); \
//                       printf s; \
//                       printf("\n");

/*********************************** Types ***********************************/
typedef unsigned int DATA;

typedef struct tree
{
    struct tree *pLeft;
    struct tree *pRight;
    DATA data;
} NODE;

/**************************** Function prototypes ****************************/
void generateDataFile(char *pFileName, int dataCount);
void readToArray(char *pFileName, DATA *pData, int size);
NODE *newNode(void);
void freeNode(NODE *pNode);
NODE *initNode(DATA d, NODE *pLeft, NODE *pRight);
NODE *createTree(DATA *pData, int i, int dataCount);
void deleteTree(NODE *pTree);
void treeInOrder(NODE *pTree);

/********************************* Functions *********************************/

/*
 * Generate data file with test data.
 */
void generateDataFile(char *pFileName, int dataCount)
{
    FILE *fp = NULL;
    DATA val = 0;

    DBG(("Creating data file %s.", DATA_FILE));
    assert((fp = fopen(DATA_FILE, "w")) != NULL);

    // Write number of elements
    DBG(("  Writing %d as number of elements.", dataCount));
    fprintf(fp, "%d ", dataCount);

    srand(time(NULL));
    //srand(0); // for debug

    for(int i = 0; i < dataCount; i++)
    {
        // Generate value in range [LIST_MIN_VAL, LIST_MAX_VAL)
        val = MIN_VAL + rand() % (MAX_VAL - MIN_VAL); 

        DBG(("  Writing element %d.", val));
        fprintf(fp, "%d ", val);
    }

    fclose(fp);
}

/*
 * Read data from file to array.
 */
void readToArray(char *pFileName, DATA *pData, int size)
{
    FILE *fp = NULL;
    int count = 0;

    DBG(("Opening file %s.", DATA_FILE));
    assert((fp = fopen(DATA_FILE, "r")) != NULL);
    
    // Read number of input values
    fscanf(fp, "%d", &count);
    DBG(("  Number of elements = %d.", count));
    
    for (int i = 0; i < count && i < size; i++)
    {
        fscanf(fp, "%d", &pData[i]);
        DBG(("  Value = %d.", pData[i]));
    }
    
    fclose(fp);
}

/*
 * Create new binary tree node.
 */
NODE *newNode(void)
{
    DBG(("Creating new node."));

    NODE *p = malloc(sizeof(NODE));

    assert(NULL != p);
    
    DBG(("  Node %x created.", p));

    return p;
}

/*
 * Delete binary tree node.
 */
void freeNode(NODE *pNode)
{
    assert(NULL != pNode);

    DBG(("Deleting node %x for element %d.",
                pNode, pNode -> data));

    free(pNode);
}

/*
 * Init new binary tree node.
 */
NODE *initNode(DATA d, NODE *pLeft, NODE *pRight)
{
    DBG(("Initializing node for element %d.", d));

    NODE *pNode = newNode();

    pNode -> data = d;

    pNode -> pLeft = pLeft;

    DBG(("  Left node %x.", pLeft));
    
    pNode -> pRight = pRight;

    DBG(("  Right node %x.", pRight));

    return pNode;
}

/*
 * Build binary tree using array elements.
 */
NODE *createTree(DATA *pData, int i, int dataCount)
{
    // Implemented via recursion.
    if (i >= dataCount)
    {
        return NULL;
    }
    else
    {
        DBG(("Creating tree for element %d (i = %d). ",
                    pData[i], i));

        return initNode(pData[i],
                createTree(pData, 2*i + 1, dataCount),
                createTree(pData, 2*i + 2, dataCount));
    }
}

/*
 * Delete binary tree.
 */
void deleteTree(NODE *pTree)
{
    // Implemented via recursion.
    if (NULL != pTree -> pLeft)
    {
        deleteTree(pTree -> pLeft);
    }
    
    if (NULL != pTree -> pRight)
    {
        deleteTree(pTree -> pRight);
    }

    freeNode(pTree);
}

/*
 * Traverse binary tree in order.
 */
void treeInOrder(NODE *pTree)
{
    if (NULL != pTree)
    {
        treeInOrder(pTree -> pLeft);

        printf("%d ", pTree -> data);

        treeInOrder(pTree -> pRight);
    }
}

int main(void)
{
    NODE *pTree = NULL;
    DATA *pData = NULL;

    generateDataFile(DATA_FILE, DATA_COUNT);

    pData = malloc(DATA_COUNT);

    readToArray(DATA_FILE, pData, DATA_COUNT);

    pTree = createTree(pData, 0, DATA_COUNT);
    
    treeInOrder(pTree);

    deleteTree(pTree);

    free(pData);

    return 0;
}
