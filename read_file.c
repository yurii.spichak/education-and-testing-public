
/*
 * This program demonstrates work with files.
 *
 * Implemented in scope of course "C for Everyone: Structured Programming"
 *     https://www.coursera.org/learn/c-structured-programming/home/info
 *
 * Author: Yurii Spichak (Lviv, Ukraine)
 * Date: Jul-13-2020
 */

#include <stdio.h>
#include <stdlib.h>

#define OUT_FILE  "answer-hw3"

int main(int argc, char* argv[])
{
    FILE* fIn = NULL;
    FILE* fOut = NULL;
    int* data = NULL;
    int size = 0;
    double avg = 0.0;
    int max = INT_MIN;
    int i = 0;

    if (2 > argc)
    {
        printf("Input file is not specified\n");
        exit(EXIT_FAILURE);
    }
    else if (2 < argc)
    {
        printf("Warning: redundant command line parameters detected"
                " (will be ignored).\n");
    }

    if ((fIn = fopen(argv[1], "r")) == NULL)
    {
        printf("Input file open error\n");
        exit(EXIT_FAILURE);
    }    
    
    if ((fOut = fopen(OUT_FILE, "w")) == NULL)
    {
        printf("Output file open error\n");
        exit(EXIT_FAILURE);
    }    

    // Read number of input values
    if (fscanf(fIn, "%d", &size) != 1)
    {
        printf("Cannot read number of input values\n");
        exit(EXIT_FAILURE);
    }

    if ((data = malloc(sizeof(int) * size)) == NULL)
    {
        printf("Memory allocation error\n");
        exit(EXIT_FAILURE);
    }

    for (i = 0; i < size; i++)
    {
        if (fscanf(fIn, "%d", &data[i]) != 1)
        {
            printf("Cannot read next value\n");
            exit(EXIT_FAILURE);
        }
        else
        {
            avg += (data[i] - avg) / (i + 1);

            if (data[i] > max)
            {
                max = data[i];
            }
        }
    }

    printf("Input values:\n");
    fprintf(fOut, "Input values:\n");
    for (i = 0; i < size; i++)
    {
        printf("%8d", data[i]);
        fprintf(fOut, "%8d", data[i]);

        if ((i + 1) % 10 == 0)
        {
            printf("\n");
            fprintf(fOut, "\n");
        }
    }
    printf("\n");
    fprintf(fOut, "\n");

    printf("Average value = %0.2f\n", avg);
    fprintf(fOut, "Average value = %0.2f\n", avg);

    printf("Max value = %d\n", max);
    fprintf(fOut, "Max value = %d\n", max);
        
    free(data);
    fclose(fIn);
    fclose(fOut);

    return 0;
}
