
/*
 * This program demonstrates the usage of doubly linked list.
 *
 * Implemented in scope of course "C for Everyone: Structured Programming"
 *     https://www.coursera.org/learn/c-structured-programming/home/info
 *
 * Author: Yurii Spichak (Lviv, Ukraine)
 * Date: Jul-13-2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/********************************** Defines **********************************/
// Number of elements in one row in program output.
#define ROW_WIDTH      5

// Number of elements in a list.
#define LIST_SIZE      200

// Data range in a list
#define LIST_MIN_VAL   0  // Including
#define LIST_MAX_VAL   50 // Excluding

/*********************************** Types ***********************************/
typedef unsigned int DATA;

typedef struct list
{
    struct list* pNext;
    struct list* pPrev;
    DATA data;
} LIST;

/**************************** Function prototypes ****************************/
DATA generateData(void);
LIST* createElement(DATA data);
void deleteElement(LIST* pCurrent);
LIST* createList(int num);
void printList(const LIST* pList, const char* pTitle);
void swapListElements(LIST* p1, LIST* p2);
LIST* sortList(LIST* pListHead);
LIST* removeDuplicates(LIST* pList);
int getListLen(const LIST* pList);

/********************************* Functions *********************************/

/*
 * Generate data for list element.
 */
DATA generateData(void)
{
    static int isInitialized = 0;

    if (0 == isInitialized)
    {
        srand(time(NULL));
        isInitialized = 1;
    }
    
    // Generate value in range [LIST_MIN_VAL, LIST_MAX_VAL)
    return LIST_MIN_VAL + rand() % (LIST_MAX_VAL - LIST_MIN_VAL);
}

/*
 * Create new element for linked list.
 */
LIST* createElement(DATA data)
{
    LIST* pList = malloc(sizeof(LIST));

    if (NULL == pList)
    {
        return NULL;
    }

    pList -> pNext = NULL;
    pList -> pPrev = NULL;
    pList -> data = data;

    return pList;
}

/*
 * Delete specified element from the list.
 */
void deleteElement(LIST* pCurrent)
{
    LIST* pPrev;
    LIST* pNext;

    if (NULL == pCurrent)
    {
        return;
    }

    pPrev = pCurrent -> pPrev;
    pNext = pCurrent -> pNext;

    free(pCurrent);

    // Check if it is not the first element of the list
    if (NULL != pPrev)
    {
        pPrev -> pNext = pNext;
    }

    // Check if it is not the last element of the list
    if (NULL != pNext)
    {
        pNext -> pPrev = pPrev;
    }
}

/*
 * Create linked list of randomly generated elements.
 */
LIST* createList(int num)
{
    int i;
    LIST* pListHead;
    LIST* pList;
    LIST* pPrev;

    if (0 >= num)
    {
        return NULL;
    }

    // Create first element
    pList = pListHead = createElement(generateData());

    // Create all other elements...
    for (i = 1; (i < num) && (NULL != pList); i++)
    {
        pList -> pNext = createElement(generateData());
        pList -> pNext -> pPrev = pList;
        pList = pList -> pNext;
    }

    return pListHead;
}

/*
 * Print list.
 */
void printList(const LIST* pList, const char* pTitle)
{
    int i = 0;

    printf("%s\n", pTitle);

    while (NULL != pList)
    {
        printf("%10d", pList -> data);

        if (++i % ROW_WIDTH == 0)
        {
            i = 0;
            printf("\n");
        }
        
        pList = pList -> pNext;
    }
}

/*
 * Get list length.
 */
int getListLen(const LIST* pList)
{
    int len = 0;

    while (NULL != pList)
    {
        len++;
        pList = pList -> pNext;
    }

    return len;

    /* // Implementation via recursion.
    if (NULL == pList)
    {
        return 0;
    }
    else
    {
        return 1 + getListLen(pList -> pNext);
    }*/
}

/*
 * Swap list elements.
 */
void swapListElements(LIST* p1, LIST* p2)
{
    DATA tmp;

    // In this implementation we just swap data.
    // TODO: optimize to swap pointers instead.
    tmp = p1 -> data;
    p1 -> data = p2 -> data;
    p2 -> data = tmp;
}

/*
 * Sort singly linked list by its data field using bubble sort algorithm.
 */
LIST* sortList(LIST* pListHead)
{
    LIST* pCurrent;
    int i, j, n;

    if (NULL == pListHead)
    {
        return NULL;
    }

    pCurrent = pListHead;
    n = getListLen(pListHead);

    // For n elements we have n - 1 comparison operations.
    for (i = 0; i < n - 1; i++)
    {
        /*
         * Each next iteration of bubble sort on input list
         * requires one comparison less since elements at the end
         * of the list are already sorted by previous iterations.
         */
        for (j = 0; j < n - i - 1; j++)
        {
            if (pCurrent -> data > pCurrent -> pNext -> data)
            {
                swapListElements(pCurrent, pCurrent -> pNext);
            }
        
            pCurrent = pCurrent -> pNext;
        }

        pCurrent = pListHead;
    }

    return pListHead;
}

/*
 * Remove all duplicate data from already sorted doubly linked list.
 */
LIST* removeDuplicates(LIST* pListHead)
{
    LIST* pList = pListHead;

    while (NULL != pList && NULL != pList -> pNext)
    {
        if (pList -> data == pList -> pNext -> data)
        {
            // Remove next element
            deleteElement(pList -> pNext);
        }
        else
        {
            pList = pList -> pNext;
        }
    }

    return pListHead;
}

int main(void)
{
    LIST* pList = createList(LIST_SIZE);

    printList(pList, "\nRandomly generated doubly linked list:");

    sortList(pList); // ignore return value

    printList(pList, "\nSorted doubly linked list:");

    removeDuplicates(pList); // ignore return value
    
    printList(pList, "\nDoubly linked list after removing all duplicate data:");

    return 0;
}
